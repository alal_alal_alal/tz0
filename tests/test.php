<?php
require 'MyQueryBuilder.php';
$config = require 'config.php';
function testSelect() {
    $config =  getconfig();
    $db = new MyQueryBuilder($config);
    $result = $db->select(['name', 'age'])->from('users')->execute();
    assertEquals($result, [['name' => 'John', 'age' => 25], ['name' => 'Sarah', 'age' => 30]]);
}

function testInsert() {
    $config =  getconfig();
    $db = new MyQueryBuilder($config);
    $result = $db->insert('users', ['name' => 'John', 'age' => 25])->execute();
    assertEquals($result, true);
}

function testUpdate() {
    $config =  getconfig();
    $db = new MyQueryBuilder($config);
    $result = $db->update('users', ['age' => 30])->where('name', '=', 'John')->execute();
    assertEquals($result, true);
}
function getconfig(){
   return   $GLOBALS['config'];
}
function testDelete() {
    $config =  getconfig();
    $db = new MyQueryBuilder($config);

    $result = $db->delete('users')->execute();
    assertEquals($result, true);
}
