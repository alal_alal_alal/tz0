<?php
require 'Helper.php';

class MyQueryBuilder
{
    private $connection;
    private $query;
    const  FIELDS = 1;
    const  COLUMNS = 2;
    private $debug_level = 2;
    private $log;

    /**
     * @var array
     */


    public function __construct($config)
    {
// создание подключения к БД
        $this->connection = new PDO("{$config['type']}:host={$config['host']};dbname={$config['database']}", $config['username'], $config['password']);
        $this->query = '';
    }

    /**
     * @param $columns -   array of columns or expressions
     * @param $escape -   need to  whether to shield? default yes
     * @return $this
     */
    public function select($columns, $escape = true)
    {
        $this->query = 'SELECT ' . ($escape ? $this->escape($columns, self::COLUMNS) : implode(', ', $columns));
        return $this;
    }

    /**
     * @param $table -  string
     * @param $escape - for complex expressions, the absence of escaping can be useful
     * @return $this
     */
    public function from($table, $escape = true)
    {
        $this->query .= ' FROM ' . ($escape ? '`' . $table . '`' : $table);
        return $this;
    }

    /**
     * @param $column -   array|string
     * @param $operator -   string
     * @param $value -   array|string
     * @return $this
     */
    public function where($column, $operator, $value)
    {
        $column = $this->escape($column, self::COLUMNS);
        $value = $this->escape($value, self::FIELDS);
        $this->query .= ' WHERE ' . $column . ' ' . $operator . ' ' . $value;
        return $this;
    }

    /**
     * @param $limit -   string
     * @return $this
     */
    public function limit($limit)
    {
        $this->query .= ' LIMIT ' . $limit;
        return $this;
    }

    /**
     * debug enabled with job positive value "$debug_level" param
     *
     * @return void
     */
    public function debug()
    {
        $db = debug_backtrace();
        $message ='';// $db[1]['function'] ?? '';
        $line = \Helper::highlightText(file($db[1]['file'])[$db[1]['line'] - 1]);
        $info = '/*'.date('d.m.Y h:i') . '*/ ' . $message . ' ' . $this->query;
        if ($this->debug_level == 1)
            $this->log[] = $info;
        if ($this->debug_level == 2)
            echo '<div class="dump"><pre><p class="src"><code class="hljs php">' . $line . '</code></p><p class="result"><code class="hljs sql">' . $info . '</code></p></pre></div>';
    }

    /**
     * @return array|false
     */
    public function execute()
    {
        $this->debug();
        $statement = $this->connection->prepare($this->query);
        $ok = $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }


    /**
     * @param $arr  array|string
     * @param $type integer
     * @param $implode -   need  escape string
     * @return array|string
     */
    public function escape($arr, $type, $implode = true)
    {
        $Arr = [];
        foreach ((array)$arr as $index => $item) {
            $Arr[$index] = ($type == 2) ? ('`' . $item . '`') : (is_null($item) ? 'NULL' : (is_numeric($item) ? $item : ($this->connection->quote($item))));
        }
        return $implode && !empty($arr) ? implode(', ', $Arr) : $Arr;
    }

    /**
     * @param $table  string
     * @param $data  array
     * @return $this
     */
    public function insert($table, $data)
    {
        $columns = $this->escape(array_keys($data), self::COLUMNS);
        $values = $this->escape(array_values($data), self::FIELDS);
        $this->query = "INSERT INTO $table ($columns) VALUES ($values)";
        return $this;
    }

    /**
     * @param $table  string
     * @param $data
     * @return $this
     */
    public function update($table, $data)
    {
        $updates = "";
        foreach ($data as $column => $value) {
            $updates .= $column . ' = ' . $value . ', ';
        }
        $updates = rtrim($updates, ', ');
        $this->query = "UPDATE $table SET $updates";
        return $this;
    }

    /**
     * @param $table  string
     * @return $this
     */
    public function delete($table)
    {
        $this->query = "DELETE FROM $table";
        return $this;
    }


    /**
     * @param $column  array|string
     * @param $direction  string
     * @return $this
     */
    public function order($column, $direction = 'ASC')
    {
        $this->query .= ' ORDER BY ' . $column . ' ' . $direction;
        return $this;
    }

    /**
     * @param $table  string
     * @param $condition  array|string
     * @return $this
     */
    public function join($table, $condition)
    {
        $this->query .= ' JOIN ' . $table . ' ON ' . $condition;
        return $this;
    }


}
