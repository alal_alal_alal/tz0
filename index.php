<?php
require 'MyQueryBuilder.php';
$config = require 'config.php';


$db = new MyQueryBuilder($config);
$result = $db->select(['name', 'age'])->from('users')->where('age', '>', 18)->limit(10)->execute();
var_dump($result);

$result = $db->insert('users', ['name' => 'John', 'age' => 25])->execute();
var_dump($result);

$result = $db->update('users', ['age' => 30])->where('name', '=', 'John')->execute();
var_dump($result);

$result = $db->delete('users')->where('name', '=', 'John')->execute();
var_dump($result);

$result = $db->select(['name', 'age'])->from('users')->order('age', 'DESC')->execute();
var_dump($result);

$result = $db->select(['users.name', 'orders.total'], false)->from('users')->join('orders', 'users.id = orders.user_id')->execute();
var_dump($result);
?>
<style type="text/css">
    .dump {
        background: #f8f8f8;
         max-width: 1200px;
        margin: auto;
        border: 1px solid #ddd;

    }

    .dump .src {
        background: #f8f8f8;
    }

    .dump .result {
        background: #2b2b2b;
        color: #f8f8f8;
        height: 2em;
    }

</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.8.0/styles/idea.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.8.0/highlight.min.js"></script>

<!-- and it's easy to individually load additional languages -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.8.0/languages/go.min.js"></script>

<script>hljs.highlightAll();</script>

